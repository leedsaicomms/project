## EvaluationDataset
For training, the input dataset has to be in form of:
```json
{"query": "What's your name?", "answer": "My name is John.", "relevant": "yes"}
{"query": "What's your name?", "answer": "I'm ok.", "relevant": "no"}
{"query": "some query sentence", "answer": "a good answer", "relevant": "yes"}
{"query": "another query sentence", "answer": "a wrong answer", "relevant": "no"}
...
```
Each data-point has to be on its own line, and every line has to contain a data-point. 
This is not supposed to be valid JSON - rather, each line is a separate JSON object in its own right.
