import csv
import numpy as np
from typing import NamedTuple, Union
from json import loads
from requests import post
from typing import Dict, List, Set, Tuple, Iterable

DataPoint = NamedTuple('DataPoint', [('query', str), ('answer', str), ('relevance', float)])
Hyperparams = NamedTuple("Hyperparams", [("threshold", float),
                                         ("max_answers_search", int),
                                         ("max_answers_relate", int),
                                         ("search_text_fuzziness", float),
                                         ])


def fetch_answers(queries: Iterable[str],
                  hyperparams: Hyperparams) \
        -> Tuple[Dict[str, List[str]], Dict[str, List[str]]]:
    """Fetches the answers to the given queries via the API.

    Args:
        queries: Queries for which answers should be fetched.
        hyperparams: Hyperparameters.

    Returns:
        a 2-tuple, with two dictionaries containing the queries as keys and a list of answers as values,
        the first of which contains search results
    """
    query_to_searched_answers = dict()  # Answers for queries as found by search module
    query_to_related_answers = dict()  # Answers related by relate module

    for query in queries:
        if query == '':
            raise KeyError("Key must not be an empty string!")

        web_req_json = {'query': query}
        search_req_json = {'query': query}

        if hyperparams.threshold:
            web_req_json['threshold'] = hyperparams.threshold

        if hyperparams.max_answers_relate:
            web_req_json['max_answers_relate'] = hyperparams.max_answers_relate

        if hyperparams.max_answers_search:
            web_req_json['max_answers_search'] = hyperparams.max_answers_search
            search_req_json['max_answers'] = hyperparams.max_answers_search

        if hyperparams.search_text_fuzziness is not None and hyperparams.search_text_fuzziness >= 0:
            web_req_json['search_text_fuzziness'] = hyperparams.search_text_fuzziness
            search_req_json['search_text_fuzziness'] = hyperparams.search_text_fuzziness

        query_to_searched_answers[query] = post('http://localhost:8002',
                                                json=search_req_json).json()['answers']

        # Retrieve answers to the given query.
        clustered_answers = post('http://localhost:8000/text',
                                 json=web_req_json).json()['answer_clusters']

        # Take answers out of clusters.
        unpacked_answers = [answer for cluster in clustered_answers for answer in cluster]

        # Sort answers by score.
        unpacked_answers = sorted(unpacked_answers, key=lambda x: np.float128(x['score']), reverse=True)

        # Only keep the text without the score.
        query_to_related_answers[query] = [ans['text'] for ans in unpacked_answers]

    return query_to_searched_answers, query_to_related_answers


def extract_evaluation_dataset(filepath: str) -> Tuple[Dict[str, Dict[str, float]], int]:
    """Extracts an evaluation dataset containing all questions with their relevant answers.

    Args:
        filepath: the file path to the file containing the full dataset from which we will perform the extraction

    Returns:
        a 2-tuple, with the first element being a dictionary having the queries as keys and a list of relevant
        answers as values, and the second argument being the total size of the answer dataset
    """
    queries_to_actual_answers_to_scores: Dict[str, Dict[str, float]] = dict()
    all_possible_answers = set()

    file_format = filepath.split('.')[1]

    num_skipped = 0

    with open(filepath, 'r', encoding='utf-8') as file:
        reader = csv.reader(file, delimiter='\t') if file_format == "tsv" else file

        for line in reader:
            # Each line in the dataset is a separate JSON object or a tab-separated line of 3 values,
            # so we convert the line to a DataPoint tuple.
            try:
                entry = DataPoint(*line) if file_format == "tsv" else DataPoint(**loads(line))
            except TypeError:
                num_skipped += 1
                continue

            # We add the answer contained within the entry to the set of all answers for tracking purposes.
            all_possible_answers.add(entry.answer)

            if entry.query not in queries_to_actual_answers_to_scores.keys():
                queries_to_actual_answers_to_scores[entry.query] = dict()

            queries_to_actual_answers_to_scores[entry.query][entry.answer] = float(entry.relevance)

    if num_skipped:
        print(f"Number of lines skipped: {num_skipped}")

    return queries_to_actual_answers_to_scores, len(all_possible_answers)


def generate_evaluation_tuples(predicted_answers: Union[List[str], Set[str]],
                               actual_answers_to_scores: Dict[str, float]) \
        -> List[Tuple[float, float]]:
    """Generates a list with the predicted and actual scores.

    Args:
        predicted_answers: a list of predicted answers
        actual_answers_to_scores: a dict mapping actual answers to their scores

    Returns:
        a list of 2-tuples, with the first element being the
            predicted score and the second being the actual score.
    """
    evaluation_tuples = []
    print(actual_answers_to_scores)
    for answer, actual_score in actual_answers_to_scores.items():
        predicted = float(answer in predicted_answers)
        evaluation_tuples.append((predicted, float(actual_score)))

    # For not specified actual answers assume it was not supposed to be predicted
    for answer in predicted_answers:
        if answer not in actual_answers_to_scores.keys():
            evaluation_tuples.append((1.0, 0))

    return evaluation_tuples
