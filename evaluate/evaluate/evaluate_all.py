"""Evaluates a combination of hyperparameters"""
import argparse
import datetime

from evaluate import evaluator, all_hyperparams
from evaluate.evaluator import Metrics, List
from evaluate.utils import Hyperparams
import os
import pandas as pd


def append_results(output_filepath,
                   output: pd.DataFrame,
                   hyperparams: Hyperparams,
                   metrics: Metrics) -> None:
    """
    Writes the evaluation results to a CSV file.

    Args:
        hyperparams: Hyper-parameters
        metrics: Evaluation results
        output_filepath: Output filepath
    """
    new_idx = len(output)
    output.loc[new_idx] = hyperparams + metrics
    output.loc[new_idx:].to_csv(output_filepath, index=False, mode='a', header=False)


def backup_file(filepath):
    last_modified_timestamp = datetime.datetime.fromtimestamp(
        os.path.getmtime(filepath)).strftime("%b-%d-%y-%H:%M:%S")
    os.rename(filepath, filepath + "_" + last_modified_timestamp)


def generate_hyperparams_list() -> List[Hyperparams]:
    """Generates a list of hyperparameters

    Returns:
        a list of all sensible hyperparameter combinations
    """
    hyperparams_list = []
    for stf in all_hyperparams.search_text_fuzziness_list:
        for t in all_hyperparams.threshold_list:
            for mar in all_hyperparams.max_answers_relate_list:
                for mas in all_hyperparams.max_answers_search_list:
                    if mar > mas:
                        # Makes no sense to try cases where MAR > MAS
                        continue
                    hyperparams_list.append(Hyperparams(t, mas, mar, stf if (stf or stf == 0) else -1))

    print(f"Generated {len(hyperparams_list)} hyperparam combinations.")
    return hyperparams_list


def main():
    parser = argparse.ArgumentParser(
        description="Evaluate AICO performance for multiple hyperparameter settings.")
    parser.add_argument("eval_file", type=str, default='eval.tsv',
                        help="path to the TSV/JSON input evaluation file containing with "
                             "'query', 'answer', 'relevant' keys/order.")
    parser.add_argument("output_file", type=str, default='results_all.json',
                        help="path to the JSON output file containing all metrics.")
    parser.add_argument("--strategy", type=str, default='any',
                        help="ambiguity strategy ('any','must_predict','must_not_predict') (default: 'any)")
    parser.add_argument("--cont", type=bool, default=True,
                        help="true, if the evaluation should be continued (default: true)")
    parser.add_argument("--backup_old_output", type=bool, default=True,
                        help="true, if old output should be backed up when not continuing (default: true)")
    args = parser.parse_args()

    output_df = pd.DataFrame(columns=Hyperparams._fields + Metrics._fields)
    if args.cont:
        try:
            output_df = pd.read_csv(args.output_file, header=0)
        except FileNotFoundError:
            output_df.to_csv(args.output_file, index=False)
    else:
        # Backup the old file if needed
        if os.path.exists(args.output_file) and args.backup_old_output:
            backup_file(args.output_file)

    hyperparams_list = generate_hyperparams_list()
    for idx, hyperparams in enumerate(hyperparams_list):
        print(f"\n=== {idx + 1}/{len(hyperparams_list)} ({100 * idx / len(hyperparams_list):.2f}%) ===")
        print(f"Threshold: {hyperparams.threshold}")
        print(f"Max answers (relate): {hyperparams.max_answers_relate}")
        print(f"Max answers (search): {hyperparams.max_answers_search}")
        print(f"Search text fuzziness: {hyperparams.search_text_fuzziness}")
        print(f"Ambiguity strategy: {args.strategy}")

        # Skip if already calculated
        if (output_df[output_df.columns[:4]] == hyperparams).all(1).any():
            print(f"Skipping {hyperparams}. Already calculated.")
            continue

        # Calculate the metrics
        metrics = evaluator.evaluate(args.eval_file, hyperparams, args.strategy)

        # Write results out to a file
        append_results(args.output_file, output_df, hyperparams, metrics)


if __name__ == '__main__':
    main()
