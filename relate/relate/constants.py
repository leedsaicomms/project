"""Constants for relate service"""
HTTP_QUERY_PARAMS_ENABLED = True
LOGGING_LEVEL = 10  # 10 - DEBUG, 20 - INFO, 30 - WARNING, 40 - ERROR

BERT_CONFIG_FILE = '/app/models/pretrained/uncased_L-12_H-768_A-12/bert_config.json'
MODEL_DIR = '/app/models/finetuned/bert-base-seq64-maxchars100'
VOCAB_FILE = '/app/models/pretrained/uncased_L-12_H-768_A-12/vocab.txt'

RELEVANT_LABEL = 'yes'
NON_RELEVANT_LABEL = 'no'

# Clustering
DEFAULT_CLUSTER_EPS = 0.015
DEFAULT_THRESHOLD = 0.7
DEFAULT_MAX_ANSWERS = 100
