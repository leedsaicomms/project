"""Contains Relater class"""
import time
from typing import List, Dict, Union
from typing import NamedTuple

import tensorflow as tf
from bert import modeling, tokenization
from bert.run_classifier import convert_examples_to_features, input_fn_builder
from sklearn.cluster import dbscan
import numpy as np

from relate import log
from relate.constants import BERT_CONFIG_FILE, VOCAB_FILE, MODEL_DIR, \
    RELEVANT_LABEL, NON_RELEVANT_LABEL, DEFAULT_CLUSTER_EPS, DEFAULT_MAX_ANSWERS, DEFAULT_THRESHOLD
from relate.model import create_examples, model_fn_builder

LOGGER = log.get_logger(__name__)
AnswerPrediction = NamedTuple('answer', [('text', str), ('score', float), ('embedding', np.array)])


class Relater:
    """Object for ranking and clustering answers according to the user provider query.

    It works by using a classifier built on top of BERT to classify query and answer pairs
    and then sorting the answers according to the score received for 'yes' (related) class.
    The sorted answers are clustered according to the embeddings.

    Attributes:
        estimator: Tensorflow estimator for classification.
        tokenizer: BERT tokenizer.
        max_seq_length: Maximum sequence length. Query + Answer will be trimmed to this many tokens.
    """

    def __init__(self,
                 bert_config_file: str = BERT_CONFIG_FILE,
                 model_dir: str = MODEL_DIR,
                 vocab_file: str = VOCAB_FILE,
                 batch_size: int = 128,
                 max_seq_length: int = 128):
        """Constructs Relater.

        Args:
            bert_config_file: Bert configuration file.
            model_dir: Model directory.
            vocab_file: Vocabulary file.
            max_seq_length: Maximum sequence length.
        """
        self.tokenizer = tokenization.FullTokenizer(
            vocab_file=vocab_file, do_lower_case=True)
        self.max_seq_length = max_seq_length

        run_config = tf.estimator.RunConfig(model_dir=model_dir)
        bert_config = modeling.BertConfig.from_json_file(bert_config_file)

        LOGGER.info("Loading estimator from '{}'...".format(MODEL_DIR))
        self.estimator = tf.estimator.Estimator(
            model_fn=model_fn_builder(bert_config),
            config=run_config,
            params={'batch_size': batch_size}
        )
        LOGGER.info("Relater is ready.")

    def _cluster(self, answer_preds: List[AnswerPrediction], eps: float = DEFAULT_CLUSTER_EPS) -> List[
        List[AnswerPrediction]]:
        """Put answers into their clusters.

        Args:
            answer_preds: Answer prediction objects.
        Returns:
            Clustered answers.
        """
        if not answer_preds:
            return []
        _, cluster_ids = dbscan(
            [a.embedding for a in answer_preds],
            eps=eps, min_samples=1,
            metric='cosine',
            n_jobs=-1)
        clusters = [[] for _ in range(np.max(cluster_ids) + 1)]
        for answer_pred, cluster_id in zip(answer_preds, cluster_ids):
            clusters[cluster_id].append(answer_pred)

        return clusters

    def relate(self, query: str,
               answers: List[str],
               threshold: float = DEFAULT_THRESHOLD,
               max_answers: int = DEFAULT_MAX_ANSWERS,
               cluster_eps: float = DEFAULT_CLUSTER_EPS) -> List[List[Dict[str, Union[str]]]]:
        """relates answers according to their relatedness to the query.

        Args:
            query: Conversation partner's query.
            answers: Potential answers.
            threshold: Threshold for the relevancy score for the answer to be considered.
            max_answers: Number of top answers to retrieve.
            cluster_eps: Epsilon value for clustering (Look up DBSCAN).
        Returns:
            Clusters of answers with their text and scores.
                Clusters are ordered with the best ones first, same for answers within the clusters.
                E.g. [[{"text": "abc", "score": "0.95"}, ...], [...],...]
        """

        if len(answers) < 2:
            # Handles zero or 1 answers gracefully
            return [[{"text": answer, "score": 1.0} for answer in answers]]

        LOGGER.info("Relating...")
        start_time = time.time()

        predict_examples = create_examples(query, answers)

        features = convert_examples_to_features(predict_examples,
                                                [RELEVANT_LABEL, NON_RELEVANT_LABEL],
                                                self.max_seq_length,
                                                self.tokenizer)
        predict_input_fn = input_fn_builder(
            features=features,
            seq_length=self.max_seq_length,
            is_training=False,
            drop_remainder=False)

        predictions = self.estimator.predict(input_fn=predict_input_fn)

        # Create answer wrappers that contain text, probability, embedding from tf prediction generator
        answer_preds = []
        for answer, pred in zip(answers, predictions):
            answer_pred = AnswerPrediction(answer, pred['probability'], pred['answer_embedding'])
            answer_preds.append(answer_pred)

        # Rank
        answer_preds = sorted(answer_preds, key=lambda x: x.score, reverse=True)

        # Discard
        answer_preds = [answer_pred for answer_pred in answer_preds if answer_pred.score >= threshold]
        answer_preds = answer_preds[:max_answers]

        # Cluster
        answer_pred_clusters = self._cluster(answer_preds, eps=cluster_eps)

        LOGGER.info("Answers w/ scores")
        for cluster_idx, cluster in enumerate(answer_pred_clusters):
            LOGGER.info("Cluster #%d: {", cluster_idx)
            for answer_pred in cluster:
                LOGGER.info("\t'%s': %f", answer_pred.text, answer_pred.score)
            LOGGER.info("}")

        # Format output
        answer_clusters = []
        for answer_pred_cluster in answer_pred_clusters:
            answer_clusters.append([{"text": answer_pred.text, "score": str(answer_pred.score)}
                                    for answer_pred in answer_pred_cluster])

        elapsed_time = time.time() - start_time
        LOGGER.info("It took %f seconds to relate.", elapsed_time)

        return answer_clusters
