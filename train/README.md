# Training
## Dataset
For training, the input dataset has to be either in form of:

**TSV:**
```tsv
some query sentence       a good answer     yes
another query sentence    a wrong answer    no 
```

The training set has to be stored at `models/train.csv`\
The development set has to be stored at `models/dev.csv`\
The evaluation set has to be stored at `models/eval.csv`

## Notes 
Because Tensorflow image for docker uses `3.5` version of Python, 
we can't use `f'` strings introduced in `3.6`.
 As an alternative use `format()` (more info [here](https://www.python.org/dev/peps/pep-0498/))