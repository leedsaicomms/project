#!/bin/bash
. /app/models/model.cfg.sh # Load settings

TRAIN_SCRIPT_PATH="/app/train/train/run_classifier.py"

python3 ${TRAIN_SCRIPT_PATH}\
  --task_name=AICO \
  --do_train=true \
  --do_eval=true \
  --data_dir=${1} \
  --data_format=${2} \
  --vocab_file=${PRETRAINED_MODEL_DIR}/vocab.txt \
  --bert_config_file=${PRETRAINED_MODEL_DIR}/bert_config.json \
  --init_checkpoint=${PRETRAINED_MODEL_DIR}/bert_model.ckpt \
  --max_seq_length=128 \
  --train_batch_size=32 \
  --learning_rate=2e-5 \
  --num_train_epochs=3.0 \
  --output_dir=${FINETUNED_MODEL_DIR}


