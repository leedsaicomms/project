#!/bin/sh
# Health checks to elasticsearch
curl -X GET "elasticsearch:9200/_cluster/health?wait_for_status=yellow&timeout=50s"

# Install/Update NLTK prerequisites
python3 -c "import nltk; nltk.download('punkt'); nltk.download('averaged_perceptron_tagger')"

# Run app as normal
gunicorn -b 0.0.0.0:8000 --reload search.api:SEARCH_API --timeout 600