import pytest
from search.utils import swap_person, question_to_statement


@pytest.mark.parametrize('text,expected',
                         [('I', 'you'),
                          ('you', 'i'),
                          ('I am', 'you are'),
                          ('you are', 'i am'),
                          ('test yourself', 'test myself'),
                          ('you are a test', 'i am a test'),
                          ('you are your test', 'i am my test'),
                          ('hey you! you are my test, and I am your test',
                           'hey i! i am your test, and you are my test'),
                          ])
def test_swap_person(text, expected):
    actual = swap_person(text)
    assert actual == expected


@pytest.mark.parametrize('text,expected', [
    ('How are you doing?', 'you are doing .'),
    ('How are you and him doing?', 'you and him are doing .'),
    ('Is the big heavy car a fun thing to drive?', 'the big heavy car is a fun thing to drive .'),
    ('Does my really good test pass?', 'my really good test does pass .'),
    ('Does this really good test pass?', 'this really good test does pass .'),
    ('Is there a number of very good tests?', 'there is a number of very good tests .'),
    ('Is here a place of very good tests?', 'here is a place of very good tests .'),
    ('Is the outside a place of very good tests?', 'the outside is a place of very good tests .'),
    ('Can you quickly be a good test.', 'you can quickly be a good test .'),
    ('I can be a test.', 'i can be a test .'),
    ('Can I be a good test.', 'i can be a good test .'),
    ('You are a test.', 'you are a test .'),
    ('You are tests.', 'you are tests .'),
    ('Are you a test?', 'you are a test .'),
    ('Are you tests?', 'you are tests .'),
    ('Are you a test? Are you?', 'you are a test . you are .'),
    ('Hey. Am I a test?', 'hey . i am a test .'),
    ('Hey. Are we tests?', 'hey . we are tests .'),
    ('Is the Universe a test?', 'the universe is a test .'),
    ('How are you testing?', 'you are testing .'),
    ('Do you test? ', 'you do test .'),
    ('Have you tested? ', 'you have tested .'),
    ('Has he tested?', 'he has tested .'),
    ('What is your test?', 'your test is .'),
    ('What is someone\'s test?', 'someone\'s test is .'),
    ('Where is your good test?', 'your good test is .'),
    ('Where is your better test?', 'your better test is .'),
    ('Where is your best test?', 'your best test is .'),
    ('Can your best test?', 'your best test can .'),
    ('This is a test.', 'this is a test .'),
    ('I can be a good test.', 'i can be a good test .'),
    ('Have I been a good test.', 'i have been a good test .'),
    ('Has he been a good test.', 'he has been a good test .'),
    ('Did he give a good test.', 'he did give a good test .'),
    ('Had he given a good test.', 'he had given a good test .'),
    ('Something can be a good test.', 'something can be a good test .'),
    ('Can something be a good test.', 'something can be a good test .'),
    ('Where can I find a very good test?', 'i can find a very good test .'),
    ('Where can I find a much better test?', 'i can find a much better test .'),
    ('Where can I find a so much better test?', 'i can find a so much better test .'),
    ('Where can I find the best test?', 'i can find the best test .'),
    ('Where can someone find a very good test?', 'someone can find a very good test .'),
    ('Do you know where I can find a very good test?', 'you do know i can find a very good test .'),
    ('Do you know where test can find a very good test?', 'you do know test can find a very good test .'),
    ('There is a very good test.', 'there is a very good test .'),
    ('There are really good tests.', 'there are really good tests .'),
    ('There are very good tests.', 'there are very good tests .'),
    ('Are there really good tests.', 'there are really good tests .'),
])
def test_question_to_statement(text, expected):
    actual = question_to_statement(text)
    assert actual.lower() == expected.lower()
