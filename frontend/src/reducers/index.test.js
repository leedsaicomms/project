import selectReducer from './index';
import {
    RESET_ALL, STORE_QUERY, GET_ANSWER_CLUSTERS
} from "../actions/types";

const initialState = {
    query: '',
    answerClusters: []
};

describe('Reduction', () => {
    it('doesn\'t update state given an invalid action', () => {
        const action = { type: 'dummy' };
        expect(selectReducer(initialState, action)).toEqual(initialState);
    });

    it('populates query data (recording)', () => {
        let payload = new Blob(["blah"]);

        const action = { type: STORE_QUERY, payload: payload };

        expect(selectReducer(initialState, action)).toEqual({
            query: action.payload,
            answerClusters: []
        });
    });

    it('populates answers array', async () => {
        const action = { type: GET_ANSWER_CLUSTERS, payload: ['answer 1', 'answer 2', 'answer 3'] };

        const state = {
            query: '',
            answerClusters: action.payload
        };

        expect(selectReducer(initialState, action)).toEqual(state);
    });

    it('resets state to initial state', () => {
        const action = {type: RESET_ALL};

        const state = {
            query: 'some bytes here',
            answerClusters: ["answer 1", "answer 2", "answer 3"]
        };

        expect(selectReducer(state, action)).toEqual(initialState);
    });
});
