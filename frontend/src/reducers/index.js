import { combineReducers } from 'redux';
import queryReducer from './queryReducer';
import answerClustersReducer from './answerClustersReducer';

export default combineReducers({
    query: queryReducer,
    answerClusters: answerClustersReducer
});