import {
    RESET_ALL,
    STORE_QUERY
} from "../actions/types";

const initialState = '';

export default function (state = initialState, action) {
    switch (action.type) {
        case STORE_QUERY:
            return action.payload;

        case RESET_ALL:
            return initialState;

        default:
            return state;
    }
}