import {storeQueryAction} from './storeQueryAction';
import {STORE_QUERY} from "./types";

describe('storeQueryAction', () => {
    it('triggers store query data redux action', () => {
        // Create a mock dispatcher
        const dispatchSpy = jest.fn();

        // Call the action
        const recordedBlob = {
            blob: new Blob(["blah"]),
            options: {
                mimeType: "audio/mp3"
            }
        };

        storeQueryAction(recordedBlob)(dispatchSpy);

        expect(dispatchSpy).toHaveBeenCalled();
        expect(dispatchSpy).toHaveBeenCalledWith({type: STORE_QUERY, payload: recordedBlob});
    });
});