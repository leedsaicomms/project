import logger from '../Logger';
import {GET_ANSWER_CLUSTERS} from "./types";
import {BACKEND_URL} from "../constants";

const realSentiment = {
    smile: 'pos',
    meh: 'neu',
    frown: 'neg'
};

/**
 * Redux action which fetches a list of clustered answers via the API
 * and dispatches the result to the redux store.
 *
 * @param inputQuery - the query either as text or a recordsing
 * @param {string} sentiment - a representation of the desired tone of answers
 * @returns {function(*): Promise<void | never>} dispatch_function - a function taking the store dispatcher as input
 */
export const getClusteredAnswersAction = (inputQuery, sentiment) => (dispatch) => {
    return fetchClusteredAnswers(inputQuery, sentiment)
        .then((answers) => {
            // This triggers the GET_ANSWER_CLUSTERS action in the query reducer,
            // payload should contain FormData with recording blob and sentiment
            logger.logInfo(`answers retrieved: ${answers}`, 'actions.getAnswersAction');
            dispatch({
                type: GET_ANSWER_CLUSTERS,
                payload: answers
            });

            return Promise.resolve();
        });
};

/**
 * Fetches clustered list of answers from the backend.
 *
 * @param inputQuery - the query either as text or a recordsing
 * @param {string} sentiment - a representation of the desired tone of answers
 * @returns {Promise<any[] | never>} answer_clusters - a promise eventually resolving to a list of answer clusters
 */
const fetchClusteredAnswers = (inputQuery, sentiment) => {
    let endpoint = null;
    let headers = {};
    let data = null;
    const answer_sentiment = realSentiment[sentiment];
    const answer_data_key = 'answer_clusters';

    if (typeof inputQuery === "string" || inputQuery instanceof String) {
        endpoint = "text";
        headers["Content-Type"] = "application/json";
        data = { query: inputQuery };
        if (answer_sentiment)
            data.sentiment = answer_sentiment;
        data = JSON.stringify(data);
    } else {
        const extension = getExtensionFromMimeType(inputQuery.options.mimeType);
        endpoint = "voice";
        data = new FormData();
        data.append("recording", inputQuery.blob, "queryAudio." + extension);
        if (answer_sentiment)
            data.append("sentiment", answer_sentiment);
    }

    return fetch(`${BACKEND_URL}/${endpoint}`, { method: 'POST', headers: headers, body: data })
        .catch((e) => {
            logger.logError('Error processing the audio. POST has been rejected, '
                + `error=${JSON.stringify(e)}`, 'actions.getClusteredAnswersAction.fetchClusteredAnswers');

            return Promise.reject(e);
        })
        .then((res) => res.json().then((body) => [res.ok, res.status, body]))
        .then(([ok, status, body]) => {
            if (!ok || !body[answer_data_key]) {
                logger.logError(`ERROR: Response status=${status}, `
                        + `Response body=${JSON.stringify(body)}`, 'actions.getClusteredAnswersAction.fetchClusteredAnswers');

                return Promise.reject(body);
            }

            logger.logInfo(`Response was HTTP_OK, Response body=${body}`,
                'actions.getAnswersAction.fetchClusteredAnswers');

            // Here we are only interested in the answer text values for each cluster,
            // so we exclude the scores at this level before storing the clusters.
            return body[answer_data_key].map(cluster => cluster.map(item => item["text"]));
        });
};

const getExtensionFromMimeType = (mimeType) =>
    mimeType.split(";")[0].split("/")[1];
