import {playSelectedAnswerAudioAction} from "./playSelectedAnswerAudioAction";
import {BACKEND_URL} from "../constants";

let mockAudio = null;

class MockAudio {
    constructor(src) {
        if (mockAudio) {
            this.onended = () => mockAudio.onended();
        } else {
            this.src = src;
            mockAudio = this;
        }
    }

    play = jest.fn().mockImplementation(() => {
        return Promise.resolve();
    })
}

global.Audio = MockAudio;

describe('playSelectedAnswerAudioAction', () => {
    it("triggers audio stream", (done) => {
        const promise = playSelectedAnswerAudioAction("test");
        mockAudio.onended();

        promise
            .then(() => {
                expect(mockAudio.src).toEqual(`${BACKEND_URL}/tts?text=test`);
                expect(mockAudio.play).toHaveBeenCalled();
                done();
            })
            .catch(done.fail);
    });
});


