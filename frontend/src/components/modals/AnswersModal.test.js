import AnswersModal from "./AnswersModal";
import React from "react";
import {MDBModal, MDBModalBody} from "mdbreact";
import ListView from "../views/ListView";

describe("AnswersModal", () => {
    it("displays modal with ListView", () => {
        const items = ["answer 1", "answer 2"];

        // Create a mock (spy) onClick handler function
        const onClickSpy = jest.fn();

        // Create an instance of the modal wrapper component
        const modalWrapper = mount(<AnswersModal isOpen={true} onClick={onClickSpy}
                                                 toggle={jest.fn()} items={items} />);

        // Assert that underlying modal and list view have been created
        expect(modalWrapper.find(MDBModal).find(MDBModalBody).find(ListView).length).toBe(1);
        expect(modalWrapper.find(MDBModal).find(MDBModalBody).find(ListView).props().items).toEqual(items);
        expect(modalWrapper.find(MDBModal).find(MDBModalBody).find(ListView).props().onClick).toEqual(onClickSpy);
    });
});