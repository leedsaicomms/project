import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {MDBBtn, MDBInput, MDBRow} from "mdbreact";
import {ReactMic} from 'react-mic'
import RecordBtn from '../buttons/RecordBtn';
import {storeQueryAction} from '../../actions/storeQueryAction.js'

/**
 * Home page component
 */
class TextHomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            queryFieldValue: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    onChange = (event) =>
        this.setState({
            queryFieldValue: event.currentTarget.value
        });

    onClick = () =>
        this.props
            .storeQueryAction(this.state.queryFieldValue)
            .then(() => {
                this.props.history.push({
                    pathname: '/sentiment'
                });
            })
            .catch(() => {
                this.props.history.push({
                    pathname: '/error'
                });
            });

    render = () =>
        <>
            <MDBRow className="row">
                <MDBInput id="queryField" label="Your Query" type="text" outline onChange={this.onChange} />
            </MDBRow>
            <MDBRow className="row" center>
                <MDBBtn id="next" disabled={!this.state.queryFieldValue || this.state.queryFieldValue === ''}
                        color="dark" onClick={this.onClick}>Next</MDBBtn>
            </MDBRow>
        </>;
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({storeQueryAction}, dispatch);

export default connect(null, mapDispatchToProps)(TextHomePage);