import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import LoadingOverlay from 'react-loading-overlay';
import {MDBBtn, MDBCol, MDBRow} from "mdbreact";
import SentimentSelectionBtn from "../buttons/SentimentSelectionBtn";
import {bindActionCreators} from 'redux';
import {getClusteredAnswersAction} from "../../actions/getClusteredAnswersAction";

/**
 * Sentiment page component
 */
class SentimentPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        };

        this.onClick = this.onClick.bind(this);
    }

    onClick(event) {
        event.preventDefault();

        const sentiment = event.currentTarget.id;

        this.setState((state) => ({
            isLoading: !state.isLoading
        }));

        this.props
            .getClusteredAnswersAction(this.props.query, sentiment)
            .then(() => {
                this.props.history.push({
                    pathname: '/answers'
                });
            })
            .catch(() => {
                this.props.history.push({
                    pathname: '/error'
                });
            });
    }

    render = () =>
        <LoadingOverlay text="Please wait..." active={this.state.isLoading} spinner>
            <MDBRow className="row" center>
                <MDBCol xs="12" md="4">
                    <SentimentSelectionBtn sentiment="smile" onClick={this.onClick} />
                </MDBCol>
                <MDBCol xs="12" md="4">
                    <SentimentSelectionBtn sentiment="meh" onClick={this.onClick} />
                </MDBCol>
                <MDBCol xs="12" md="4">
                    <SentimentSelectionBtn sentiment="frown" onClick={this.onClick} />
                </MDBCol>
            </MDBRow>
            <MDBRow className="row" center>
                <MDBBtn color="dark" onClick={this.onClick}>I'm not sure</MDBBtn>
            </MDBRow>
        </LoadingOverlay>;
}

SentimentPage.propTypes = {
    query: PropTypes.any.isRequired
};

const mapStateToProps = (state) => ({
    query: state.query
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({getClusteredAnswersAction}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SentimentPage);