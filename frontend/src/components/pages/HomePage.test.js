import React from 'react';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from "redux";
import PropTypes from 'prop-types';
import HomePage from './HomePage';
import rootReducer from "../../reducers";
import thunk from "redux-thunk";
import {MDBBtn} from "mdbreact";
import RecordBtn from "../buttons/RecordBtn";

describe('Homepage', () => {
    let home, wrapper, store;

    beforeEach(() => {
        store = createStore(
            rootReducer,
            applyMiddleware(thunk)
        );

        const options = {
            childContextTypes: {
                query: PropTypes.object.isRequired
            }
        };

        wrapper = mount(<Provider store={store}><HomePage /></Provider>, options);
        home = wrapper.find('HomePage');
    });

    it("should render recording button", () => {
        expect(home.find(RecordBtn).length).toBe(1);
    });

    it("record button on click should change button text", () => {
        // Check the current text
        expect(home.find(MDBBtn).text()).toEqual(" Start Recording");

        // Simulate a click
        home.find(MDBBtn)
            .simulate('click');

        // Check that the text changed
        expect(home.find(MDBBtn).text()).toEqual(" Stop Recording");

        // Simulate a click
        home.find(MDBBtn)
            .simulate('click');

        // Check that the text changed
        expect(home.find(MDBBtn).text()).toEqual(" Start Recording");
    });

});