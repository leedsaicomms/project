import React, {Component} from 'react';
import {MDBAlert, MDBBtn, MDBRow} from "mdbreact";

/**
 * Error page component
 */
export default class ErrorPage extends Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    onClick = () =>
        this.props.history.goBack();

    render = () =>
        <>
            <MDBRow className="row">
                <MDBAlert color="danger" center>
                    <h4 className="alert-heading">Uh oh!</h4>
                    <p>Sorry, but we weren't able to service your request.</p>
                    <hr />
                    <p className="mb-0">You may retry by clicking the button below.</p>
                </MDBAlert>
            </MDBRow>
            <MDBRow className="row" center>
                <MDBBtn color="dark" onClick={this.onClick}>
                    <i className="fas fa-arrow-left fa-fw" /> Go Back & Retry
                </MDBBtn>
            </MDBRow>
        </>;
}