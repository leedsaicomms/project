import React from 'react';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from "redux";
import PropTypes from 'prop-types';
import SentimentPage from './SentimentPage';
import rootReducer from "../../reducers";
import thunk from "redux-thunk";
import {createMemoryHistory} from "history";
import SentimentSelectionBtn from "../buttons/SentimentSelectionBtn";
import {MDBBtn} from "mdbreact";
import {STORE_QUERY, STORE_SELECTED_RESPONSE_TEXT} from "../../actions/types";

describe('SentimentPage', () => {
    let sentimentPage, wrapper, store, testHistory;

    beforeEach(() => {
        store = createStore(
            rootReducer,
            applyMiddleware(thunk)
        );

        const options = {
            childContextTypes: {
                query: PropTypes.object.isRequired,
                history: PropTypes.object.isRequired,
            }
        };

        // mock out the fetch call
        jest.spyOn(global, "fetch").mockImplementation(() => Promise.resolve({
            ok: true,
            json: () => Promise.resolve({'answer_clusters': [
                    [
                        {"text": "answer 1", "score": "0.99"},
                        {"text": "answer 2", "score": "0.97"},
                    ],
                    [
                        {"text": "answer 3", "score": "0.98"}
                    ]
                ]})}));

        testHistory = createMemoryHistory();
        testHistory.push({
            pathname: '/sentiment'
        });

        wrapper = mount(<Provider store={store}>
            <SentimentPage history={testHistory} />
        </Provider>, options);
        sentimentPage = wrapper.find('SentimentPage');
    });

    it("fetches state from store", () => {
        // Assert initial state
        expect(sentimentPage.props().query).toBe('');

        // Simulate reducers by populating the store with some data
        store.dispatch({
            type: STORE_QUERY,
            payload: "What's your name?"
        });

        wrapper = wrapper.update();
        sentimentPage = wrapper.find('SentimentPage');

        // Assert that the state has been updated
        expect(sentimentPage.props().query).toEqual("What's your name?");
    });

    it("should render sentiment buttons", () => {
        expect(sentimentPage.find(SentimentSelectionBtn).length).toBe(3);
    });

    it("sentiment buttons on click should change button text", (done) => {
        // Preparation Stage - populate query recording state
        store.dispatch({
            type: STORE_QUERY,
            payload: {
                blob: new Blob(["blah"]),
                options: {
                    mimeType: "audio/webm"
                }
            }
        });

        // Simulate a click
        sentimentPage.find(MDBBtn)
            .at(1)
            .simulate('click');

        wrapper = wrapper.update();
        sentimentPage = wrapper.find('SentimentPage');

        // Assert that answers page is now open
        setTimeout(() => {
            expect(testHistory.entries.length).toBe(3);
            expect(testHistory.entries[0].pathname).toEqual("/");
            expect(testHistory.entries[1].pathname).toEqual("/sentiment");
            expect(testHistory.entries[2].pathname).toEqual("/answers");
            expect(testHistory.location.pathname).toEqual("/answers");
            done();
        }, 2000);
    });

    it("sentiment buttons onClick should change isLoading state", () => {
        expect(sentimentPage.state().isLoading).toBe(false);

        // Preparation Stage - populate query recording state
        store.dispatch({
            type: STORE_QUERY,
            payload: {
                blob: new Blob(["blah"]),
                options: {
                    mimeType: "audio/webm"
                }
            }
        });

        // Simulate a click
        sentimentPage.find(MDBBtn)
            .at(1)
            .simulate('click');

        // Assert that answers page is now open
        wrapper = wrapper.update();
        sentimentPage = wrapper.find('SentimentPage');

        expect(sentimentPage.state().isLoading).toBe(true);
    });
});