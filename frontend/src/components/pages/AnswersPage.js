import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {playSelectedAnswerAudioAction} from "../../actions/playSelectedAnswerAudioAction";
import {resetAction} from "../../actions/resetAction";
import LoadingOverlay from 'react-loading-overlay';
import AnswersModal from "../modals/AnswersModal";
import AnswerClusterSelectionView from "../views/AnswerClusterSelectionView";

/**
 * Answers page component
 */
class AnswersPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isAnswerModalOpen: false,
            selectedCluster: []
        };

        this.onClusterSelection = this.onClusterSelection.bind(this);
        this.onAnswerSelection = this.onAnswerSelection.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }

    onClusterSelection(event) {
        event.preventDefault();

        const selectedClusterId = event.currentTarget.id;

        this.setState((state) => ({
            selectedCluster: this.props.answerClusters[selectedClusterId],
        }));

        this.toggleModal();
    }

    onAnswerSelection(event) {
        event.preventDefault();

        const selectedAnswer = event.currentTarget.textContent;

        event.currentTarget.className += " active";

        this.setState((state) => ({
            isLoading: !state.isLoading
        }));

        this.toggleModal();

        playSelectedAnswerAudioAction(selectedAnswer)
            .then(this.props.resetAction)
            .then(() => {
                this.props.history.push({
                    pathname: '/'
                });
            })
            .catch(() => {
                this.props.history.push({
                    pathname: '/error'
                });
            });
    }

    toggleModal = () =>
        this.setState((state) => ({
            isAnswerModalOpen: !state.isAnswerModalOpen
        }));

    render = () =>
        <LoadingOverlay text="Please wait..." active={this.state.isLoading} spinner>
            <AnswerClusterSelectionView onClick={this.onClusterSelection} clusters={this.props.answerClusters} />
            <AnswersModal isOpen={this.state.isAnswerModalOpen} onClick={this.onAnswerSelection}
                          toggle={this.toggleModal} items={this.state.selectedCluster} />
        </LoadingOverlay>;
}

AnswersPage.propTypes = {
    answerClusters: PropTypes.array.isRequired
};

const mapStateToProps = (state) => ({
    answerClusters: state.answerClusters
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({resetAction}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AnswersPage);