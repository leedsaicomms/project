import React from 'react';
import {MDBBtn} from "mdbreact";
import RecordBtn from './RecordBtn';


describe('RecordBtn', () => {
    let onClickSpy, isRecording, recordBtnWrapper;

    beforeEach(() => {
        // Create a 'spy' (akin to a mock object)
        onClickSpy = jest.fn();
        isRecording = false;
        // Create the record button object, passing the properties
        recordBtnWrapper = mount(<RecordBtn isRecording={isRecording} onClick={onClickSpy}/>);
    });

    it('renders an MDBBtn', () => {
        // Check that a single MDBBtn exists
        expect(recordBtnWrapper.find(MDBBtn).length).toEqual(1);
    });

    it('initially says "Start Recording"', () => {
        // Check that the button says 'Start Recording'
        expect(recordBtnWrapper.find(MDBBtn).text()).toEqual(" Start Recording");
    });

    it('on click triggers on click action', () => {
        // Simulate a click
        recordBtnWrapper
            .find(MDBBtn)
            .simulate('click');

        // Check that start recording action was called
        expect(onClickSpy.mock.calls.length).toBe(1);
    });

    it('renders "Stop Recording" text if isRecording is true', () => {
        isRecording = true;
        // Recreate the record button object, passing the properties
        recordBtnWrapper = mount(<RecordBtn isRecording={isRecording} onClick={onClickSpy}/>);

        // Check that the button says 'Stop Recording'
        expect(recordBtnWrapper.find(MDBBtn).text()).toEqual(" Stop Recording");
    });
});