import React from 'react'
import {MDBBtn} from "mdbreact";
import PropTypes from "prop-types";

export default class SentimentSelectionBtn extends React.Component {
    static propTypes = {
        sentiment: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired
    };

    render = () =>
        <MDBBtn id={this.props.sentiment} color="dark" onClick={this.props.onClick}>
            <i className={`far fa-${this.props.sentiment}`} />
        </MDBBtn>;
}