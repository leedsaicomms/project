import React, { Component } from 'react';
import './App.css';

import { HashRouter as Router, Route } from 'react-router-dom';
import {Provider} from 'react-redux';
import store from "./store";

import {MDBContainer} from "mdbreact";
import HomePage from "./components/pages/HomePage";
import SentimentPage from "./components/pages/SentimentPage";
import AnswersPage from './components/pages/AnswersPage';
import ErrorPage from "./components/pages/ErrorPage";
import TextHomePage from "./components/pages/TextHomePage";

class App extends Component {
    render = () =>
        <Provider store={store}>
            <Router>
                <MDBContainer className="container-fluid">
                    <div id="app">
                        <Route exact path="/" component={HomePage} />
                        <Route exact path="/text" component={TextHomePage} />
                        <Route exact path="/sentiment" component={SentimentPage} />
                        <Route exact path="/answers" component={AnswersPage} />
                        <Route exact path="/error" component={ErrorPage} />
                    </div>
                </MDBContainer>
            </Router>
        </Provider>;
}

export default App;