import React from "react";
import Enzyme, {shallow, render, mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {createSerializer} from "enzyme-to-json";
import sinon from "sinon";

// Set the default serializer for Jest to be the from enzyme-to-json
// This produces an easier to read (for humans) serialized format.
expect.addSnapshotSerializer(createSerializer({mode: "deep"}));

// React 16 Enzyme adapter
Enzyme.configure({adapter: new Adapter()});

// Define globals to cut down on imports in test files
global.React = React;
global.shallow = shallow;
global.render = render;
global.mount = mount;
global.sinon = sinon;

// The follow along with the __mocks__/react-mic.js avoids loading the ReactMic module & stops errors from react-mic/es/libs/AudioContext
global.window.AudioContext = jest.fn().mockImplementation(() => ({
    createAnalyser: jest.fn()
}));
