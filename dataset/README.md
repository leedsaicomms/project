Dataset Generator
===

This module automatically generates positive and negative relevancy samples
from a reddit dataset, and writes them to training and test files in a
specified format.

Setup
===

Ensure that the unprocessed reddit dataset is downloaded (torrent can be
found [here](https://www.reddit.com/r/datasets/comments/3bxlg7/i_have_every_publicly_available_reddit_comment/)).

Running
===

To generate the samples, run the following command in the 'dataset/dataset' directory:

```sh
python3 gen_samples.py INPUT_FILE
```
Capitalized names are arguments that should be specified by the user.

Positional arguments
===

**INPUT_FILE**: path to the JSON input file

Optional arguments
===

**-h, --help**: show this help message and exit

**-num_lines NUM_LINES**: number of lines to process in total (default: all lines)

**-chunk_size CHUNK_SIZE**: number of lines to process at a time (default: all lines)

**-train TRAIN**: ratio of samples to use for training (default: 0.8)

**-dev DEV**: ratio of samples to use for validation (default: 0.1)

**-test TEST**: ratio of samples to use for testing (default: 0.1)

**-neg_ratio NEG_RATIO**: number of negative samples to generate per positive (default: 1)

**-max_chars MAX_CHARS**: character limit on comments to use in samples (default: no limit)

**-seed SEED**: seed for random operations (default: random)

**-format FORMAT**: format to output samples in (json, csv, tsv) (default: json)

**-v**: verbose
