import os
from typing import List, Dict

import pytest
import rapidjson

from dataset.dataset_generator import DatasetGenerator

TEST_PATH = os.path.dirname(os.path.abspath(__file__))
MOCK_DATASET = TEST_PATH + "/mock_reddit_dataset.json"
THREAD_MOCK_DATASET = TEST_PATH + "/mock_reddit_thread_dataset.json"



class TestDatasetGenerator:
    """Test class for the DatasetGenerator class."""

    def test_filename_invalid(self):
        """Tests whether the class handles invalid dataset file names gracefully."""
        with pytest.raises(FileNotFoundError):
            _ = DatasetGenerator("invalid file", 30)

    def test_num_lines_invalid(self):
        """Tests whether the class handles invalid number of num_lines values (out of range) gracefully."""
        # Negative
        with pytest.raises(ValueError) as error:
            _ = DatasetGenerator(MOCK_DATASET, -1)
        assert "'num_lines' parameter must be positive" in str(error.value)

        # Zero
        with pytest.raises(ValueError) as error:
            _ = DatasetGenerator(MOCK_DATASET, 0)
        assert "'num_lines' parameter must be positive" in str(error.value)

    def test_chunk_size_invalid(self):
        """Tests whether the class handles invalid chunk size values gracefully."""
        # Negative
        with pytest.raises(ValueError) as error:
            _ = DatasetGenerator(MOCK_DATASET, 30, -1)
        assert "'lines_per_chunk' parameter must be positive" in str(error.value)

        # Zero
        with pytest.raises(ValueError) as error:
            _ = DatasetGenerator(MOCK_DATASET, 30, 0)
        assert "'lines_per_chunk' parameter must be positive" in str(error.value)

        # Larger than num_lines parameter
        with pytest.raises(ValueError) as error:
            _ = DatasetGenerator(MOCK_DATASET, 10, 11)
        assert "'lines_per_chunk' parameter must be <= 'num_lines' parameter" in str(error.value)



    def test_thread_sorting(self, tmp_path):
        """Test that sorting the dataset by reddit thread works as intended."""
        dg = DatasetGenerator(THREAD_MOCK_DATASET, 15)
        dg.sort_input_file_by_reddit_threads(tmp_path / "sorted.json")

        expected = "{\"id\":\"1\",\"body\":\"test1\",\"link_id\":\"t3_1\"}\n" \
                   "{\"id\":\"4\",\"body\":\"test4\",\"link_id\":\"t3_1\"}\n" \
                   "{\"id\":\"12\",\"body\":\"test12\",\"link_id\":\"t3_1\"}\n" \
                   "{\"id\":\"14\",\"body\":\"test14\",\"link_id\":\"t3_1\"}\n" \
                   "{\"id\":\"15\",\"body\":\"test15\",\"link_id\":\"t3_1\"}\n" \
                   "{\"id\":\"2\",\"body\":\"test2\",\"link_id\":\"t3_4\"}\n" \
                   "{\"id\":\"3\",\"body\":\"test3\",\"link_id\":\"t3_6\"}\n" \
                   "{\"id\":\"5\",\"body\":\"test5\",\"link_id\":\"t3_2\"}\n" \
                   "{\"id\":\"8\",\"body\":\"test8\",\"link_id\":\"t3_2\"}\n" \
                   "{\"id\":\"10\",\"body\":\"test10\",\"link_id\":\"t3_2\"}\n" \
                   "{\"id\":\"6\",\"body\":\"test6\",\"link_id\":\"t3_5\"}\n" \
                   "{\"id\":\"9\",\"body\":\"test9\",\"link_id\":\"t3_5\"}\n" \
                   "{\"id\":\"11\",\"body\":\"test11\",\"link_id\":\"t3_5\"}\n" \
                   "{\"id\":\"7\",\"body\":\"test7\",\"link_id\":\"t3_3\"}\n" \
                   "{\"id\":\"13\",\"body\":\"test13\",\"link_id\":\"t3_3\"}\n"

        with open(tmp_path / "sorted.json", "r") as file:
            actual = file.read()
            assert actual == expected
