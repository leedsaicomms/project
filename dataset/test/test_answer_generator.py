import os

import pytest

from dataset.gen_answers import AnswerGenerator

TEST_PATH = os.path.dirname(os.path.abspath(__file__))
MOCK_DATASET = TEST_PATH + "/mock_reddit_dataset.json"

COMMENT_BODY = \
    {0: "test1",
     1: "test2 long",
     2: "test3",
     3: "test4 long",
     4: "test5",
     5: "test6 long",
     6: "test7",
     7: "test8 long",
     8: "test9",
     9: "test10 long",
     10: "test11",
     11: "test12 long",
     12: "test13",
     13: "test14 long",
     14: "test15",
     15: "test16 long",
     16: "test17",
     17: "test18 long",
     18: "test19",
     19: "test20 long",
     20: "test21",
     21: "test22 & long",
     22: "test23",
     23: "test24 < long",
     24: "test25",
     25: "test26 > long",
     26: "test27",
     27: "test28 long",
     28: "test29",
     29: "test30 long"}


class TestAnswerGenerator:
    def test_n_processes_invalid(self):
        """Tests whether the class handles invalid number of processes gracefully"""

        dg = AnswerGenerator(MOCK_DATASET, 30)

        with pytest.raises(ValueError):
            dg.generate_answers(output_dir="test.out", n_processes=0)

        with pytest.raises(ValueError):
            dg.generate_answers(output_dir="test.out", n_processes=-1)

    def test_answers(self, tmp_path):
        """Tests that all answers are generated."""
        dg = AnswerGenerator(MOCK_DATASET, 30)
        dg.generate_answers(tmp_path)

        for answer in dg.answers:
            assert answer["text"] in COMMENT_BODY.values()
            assert -1 <= answer["sentiment"] <= 1

    def test_answers_threaded(self, tmp_path):
        """Tests that all answers are generated when using multiple threads."""
        dg = AnswerGenerator(MOCK_DATASET, 30)
        dg.generate_answers(tmp_path, n_processes=4)

        for answer in dg.answers:
            assert answer["text"] in COMMENT_BODY.values()
            assert -1 <= answer["sentiment"] <= 1
